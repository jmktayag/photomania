//
//  PhotosCDTVC.m
//  Photomania
//
//  Created by Mckein on 12/22/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import "PhotosCDTVC.h"
#import "Photo.h"
#import "ImageViewController.h"

@interface PhotosCDTVC ()

@end

@implementation PhotosCDTVC


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Photo Cell"];
    
    Photo *photo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = photo.title;
    cell.detailTextLabel.text = photo.subtitle;
    
    return cell;
}

#pragma mark - Navigation
-(void)prepareViewController:(id)vc forSegue:(NSString *)segueIdentifier fromIndexPath:(NSIndexPath *) indexPath
{
    Photo *photo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    if ([vc isKindOfClass:[ImageViewController class]]){
        //prepare vc
        ImageViewController *ivc = (ImageViewController *) vc;
        ivc.imageURL =[NSURL URLWithString:photo.imageURL];
        ivc.title = photo.title;
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = nil;
    if ([sender isKindOfClass:[UITableViewCell class]]){
        indexPath = [self.tableView indexPathForCell:sender];
    }
    [self prepareViewController:segue.destinationViewController
                       forSegue:segue.identifier
                  fromIndexPath:indexPath];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    id detailsvc = [self.splitViewController.viewControllers lastObject];
    if ([detailsvc isKindOfClass:[UINavigationController class]]){
        detailsvc = [((UINavigationController *)detailsvc).viewControllers firstObject];
        [self prepareViewController:detailsvc
                           forSegue:nil
                      fromIndexPath:indexPath];
    }
}
@end
