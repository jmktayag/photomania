//
//  PhotoDatabaseAvailability.h
//  Photomania
//
//  Created by Mckein on 12/20/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#ifndef Photomania_PhotoDatabaseAvailability_h
#define Photomania_PhotoDatabaseAvailability_h

#define PhotoDatabaseAvailabilityNotification @"PhotoDatabaseAvailabilityNotification" //name of the notification
#define PhotoDatabaseAvailabilityContext  @"Context" //key of the dictionary in the notification

#endif
