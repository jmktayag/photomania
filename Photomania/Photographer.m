//
//  Photographer.m
//  Photomania
//
//  Created by Mckein on 12/19/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import "Photographer.h"
#import "Photo.h"


@implementation Photographer

@dynamic name;
@dynamic photos;

@end
