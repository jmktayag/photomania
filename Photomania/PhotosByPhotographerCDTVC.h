//
//  PhotosByPhotographerCDTVC.h
//  Photomania
//
//  Created by Mckein on 12/22/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import "CoreDataTableViewController.h"
#import "Photographer.h"
#import "PhotosCDTVC.h"

@interface PhotosByPhotographerCDTVC : PhotosCDTVC

@property (nonatomic, strong) Photographer *photographer; //pass photographer data here

@end
