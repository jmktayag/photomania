//
//  ImageViewController.m
//  Imaginarium
//
//  Created by Mckein on 12/11/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import "ImageViewController.h"
#import "URLViewController.h"

@interface ImageViewController ()<UIScrollViewDelegate, UISplitViewControllerDelegate>
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIImage *image;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (weak, nonatomic) UIPopoverController *urlPopOverViewController;

@end

@implementation ImageViewController


//outlet setup doesn't actually run in preparesegue, so you need to set in setter
-(void)setScrollView:(UIScrollView *)scrollView
{
    _scrollView = scrollView;
    _scrollView.minimumZoomScale = 0.2;
    _scrollView.maximumZoomScale = 2;
    _scrollView.delegate = self;
     self.scrollView.contentSize = self.image ? self.image.size : CGSizeZero;

}

#pragma mark - UIScrollView Delegate
-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}

-(void)setImageURL:(NSURL *)imageURL
{
    _imageURL = imageURL;
    //self.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:self.imageURL]]; //this blocks the UI
    //Multithreading call here
    [self startDownloadingImage];
}

//delegate to check existing download task
-(void) startDownloadingImage
{
    self.image = nil;
    if (self.imageURL){
        [self.spinner startAnimating];
        //Downloading of Image
        NSURLRequest *request = [NSURLRequest requestWithURL:self.imageURL];
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration ephemeralSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
        NSURLSessionDownloadTask *task = [session downloadTaskWithRequest:request
                                                        completionHandler:^(NSURL *localfile, NSURLResponse *response, NSError *error) {
                                                            if (!error){
                                                                if ([request.URL isEqual:self.imageURL]){
                                                                    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:localfile]]; //off the main view possible with uiimage
                                                                    //method to display in the image must be in the main thread
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                        self.image = image;
                                                                    });
                                                                    
                                                                    //another way
                                                                   // [self performSelectorOnMainThread:@selector(setImage:) withObject:image waitUntilDone:NO];
                                                                }
                                                            }
                                                        }]; //double click will comple the params
        [task resume];
    }
}

-(UIImageView *)imageView
{
    if (!_imageView) _imageView = [[UIImageView alloc] init];
    return _imageView;
}

//do not need to synthesize because you don't use _image instance variable
-(UIImage *)image
{
    return self.imageView.image;
}

-(void)setImage:(UIImage *)image
{
    
    self.imageView.image = image; //does not change the frame of the UIImageView
    //[self.imageView sizeToFit]; //update the frame of the UIView
    self.imageView.frame = CGRectMake(0, 0, image.size.width, image.size.width);
    self.scrollView.contentSize = self.image ? self.image.size : CGSizeZero;
    [self.urlPopOverViewController dismissPopoverAnimated:YES];
    [self.spinner stopAnimating];
    
}

#pragma mark - UISplitViewControllerDelegate

-(void)awakeFromNib
{
    self.splitViewController.delegate = self;
}

-(BOOL)splitViewController:(UISplitViewController *)svc
  shouldHideViewController:(UIViewController *)vc
             inOrientation:(UIInterfaceOrientation)orientation
{
    //will hide when in portrait mode
    return UIInterfaceOrientationIsPortrait(orientation);
}

-(void)splitViewController:(UISplitViewController *)svc
    willHideViewController:(UIViewController *)aViewController
         withBarButtonItem:(UIBarButtonItem *)barButtonItem
      forPopoverController:(UIPopoverController *)pc
{
    barButtonItem.title = aViewController.title; //master title -- you must set the title of the master view controller to display the master
    self.navigationItem.leftBarButtonItem = barButtonItem; //set the bar button to show the master
}

-(void)splitViewController:(UISplitViewController *)svc
    willShowViewController:(UIViewController *)aViewController
 invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    self.navigationItem.leftBarButtonItem = nil;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    [self.scrollView addSubview:self.imageView];
}

#pragma mark - Navigation
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Show URL"]){
        if ([segue.destinationViewController isKindOfClass:[URLViewController class]]){
            URLViewController *urlvc = (URLViewController *) segue.destinationViewController;
            if ([segue isKindOfClass:[UIStoryboardPopoverSegue class]]){
                UIStoryboardPopoverSegue *popOverSegue = (UIStoryboardPopoverSegue *) segue; //if segue is popover
                self.urlPopOverViewController = popOverSegue.popoverController;
            }
            
            urlvc.url = self.imageURL;
            
        }
    }
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"Show Url"]){
        return self.urlPopOverViewController ? NO : (self.imageURL ? YES : NO);
    }else{
        return [super shouldPerformSegueWithIdentifier:identifier sender:sender];
    }
}






@end
