//
//  URLViewController.m
//  Photomania
//
//  Created by Mckein on 12/22/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import "URLViewController.h"

@interface URLViewController ()
@property (weak, nonatomic) IBOutlet UITextView *urlTextView;

@end

@implementation URLViewController

-(void)setUrl:(NSURL *)url
{
    _url = url;
    [self updateUI];
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    [self updateUI];
}

-(void) updateUI
{
    self.urlTextView.text = [self.url absoluteString];
}
@end
