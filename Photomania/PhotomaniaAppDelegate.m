//
//  PhotomaniaAppDelegate.m
//  Photomania
//
//  Created by Mckein on 12/19/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import "PhotomaniaAppDelegate.h"
#import "FlickrFetcher.h"
#import "Photo+Flickr.h"
#import "PhotomaniaAppDelegate+MOC.h"
#import "PhotoDatabaseAvailability.h"

@interface PhotomaniaAppDelegate()<NSURLSessionDelegate>

@property (copy, nonatomic) void (^flickrDownloadBackgroundURLSessionCompletionHandler)(); //all block shoulf be copy property
@property (strong, nonatomic) NSURLSession *flickrDownloadSession;
@property (strong, nonatomic) NSTimer *flickrForeGroundFetcher;
@property (strong, nonatomic) NSManagedObjectContext *photoDatabaseContext;

@end

//Name of the flickr fetching background session
#define FLICKR_FETCH @"Flickr Just Uploaded Fetch"

//how often in seconds we fetch new photos  if we are in the foreground
#define FOREGROUND_FLICKR_FETCH_INTERVAL (20*60)

// how long we'll wait for a Flickr fetch to return when we're in the background
#define BACKGROUND_FLICKR_FETCH_TIMEOUT (10)

@implementation PhotomaniaAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    // when we're in the background, fetch as often as possible (which won't be much)
    [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    
    self.photoDatabaseContext = [self createMainQueueManagedObjectContext];
    
    //background fetch
    [self startFlickrFetch];
    return YES;
}

//perform code when app is in background - must enable background thread in CAPABILITIES tab
-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    if (self.photoDatabaseContext) {
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration ephemeralSessionConfiguration]; //wait for download and update to UI
        sessionConfig.allowsCellularAccess = NO;
        sessionConfig.timeoutIntervalForRequest = BACKGROUND_FLICKR_FETCH_TIMEOUT; // want to be a good background citizen!
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[FlickrFetcher URLforRecentGeoreferencedPhotos]];
        NSURLSessionDownloadTask *task = [session downloadTaskWithRequest:request
                                                        completionHandler:^(NSURL *localFile, NSURLResponse *response, NSError *error) {
                                                            if (error) {
                                                                NSLog(@"Flickr background fetch failed: %@", error.localizedDescription);
                                                                completionHandler(UIBackgroundFetchResultNoData);
                                                            } else {
                                                                [self loadFlickrPhotosFromLocalURL:localFile
                                                                                       intoContext:self.photoDatabaseContext
                                                                               andThenExecuteBlock:^{
                                                                                   completionHandler(UIBackgroundFetchResultNewData);
                                                                               }
                                                                 ];
                                                            }
                                                        }];
        [task resume];
    } else {
        completionHandler(UIBackgroundFetchResultNoData); // no app-switcher update if no database!
    }

}


//called whenever things happen in the background
-(void)application:(UIApplication *)application
handleEventsForBackgroundURLSession:(NSString *)identifier
 completionHandler:(void (^)())completionHandler
{
    //call this when we're done
    self.flickrDownloadBackgroundURLSessionCompletionHandler = completionHandler;
}

-(void)setPhotoDatabaseContext:(NSManagedObjectContext *)photoDatabaseContext
{
    _photoDatabaseContext = photoDatabaseContext;
    
    //Foreground fetch
   [NSTimer scheduledTimerWithTimeInterval:FOREGROUND_FLICKR_FETCH_INTERVAL
                                    target:self
                                  selector:@selector(startFlickrFetch:)
                                  userInfo:nil repeats:YES];
    
    
    
    //Post notification to send on the PhotographersCDTVC
    NSDictionary *userInfo = self.photoDatabaseContext ? @{PhotoDatabaseAvailabilityContext: self.photoDatabaseContext } : nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:PhotoDatabaseAvailabilityNotification
                                                        object:self
                                                      userInfo:userInfo];
}

// gets the Flickr photo dictionaries out of the url and puts them into Core Data
// this was moved here after lecture to give you an example of how to declare a method that takes a block as an argument
// and because we now do this both as part of our background session delegate handler and when background fetch happens

- (void)loadFlickrPhotosFromLocalURL:(NSURL *)localFile
                         intoContext:(NSManagedObjectContext *)context
                 andThenExecuteBlock:(void(^)())whenDone
{
    if (context) {
        NSArray *photos = [self flickrPhotosAtUrl:localFile];
        [context performBlock:^{
            [Photo loadPhotosFromFlickrArray:photos intoManagedObjectContext:context];
            [context save:NULL]; // NOT NECESSARY if this is a UIManagedDocument's context
            if (whenDone) whenDone();
        }];
    } else {
        if (whenDone) whenDone();
    }
}

//NSTimerFlickr fetch
-(void) startFlickrFetch:(NSTimer *) timer
{
    [self startFlickrFetch];
}

//Fetching mechanism for downloading flickr data to database
-(void) startFlickrFetch
{
    [self.flickrDownloadSession getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
        if (![downloadTasks count]){
            NSURLSessionDownloadTask *task = [self.flickrDownloadSession downloadTaskWithURL:[FlickrFetcher URLforRecentGeoreferencedPhotos]];
            task.taskDescription = FLICKR_FETCH;
            [task resume];
        }else{
            for (NSURLSessionDownloadTask *task in downloadTasks) {
                [task resume];
            }
        }
    }];
}

-(NSURLSession *)flickrDownloadSession
{
    if (!_flickrDownloadSession){
        static dispatch_once_t onceToken;
        // dispatch_once ensures that the block will only ever get executed once per application launch
        dispatch_once(&onceToken, ^{
            NSURLSessionConfiguration *urlSessionConfig = [NSURLSessionConfiguration backgroundSessionConfiguration:FLICKR_FETCH];
            urlSessionConfig.allowsCellularAccess = NO; //for example we will not use cellular network
            _flickrDownloadSession = [NSURLSession sessionWithConfiguration:urlSessionConfig
                                                                   delegate:self
                                                              delegateQueue:nil]; //delegate is set to self because if we the flick fetcher did finish in background we want the application to launch and tell us
        });
    }
    return _flickrDownloadSession;

}

-(NSArray *) flickrPhotosAtUrl:(NSURL *) url
{
    NSData *flickrJSONData = [NSData dataWithContentsOfURL:url]; //download the json request
    NSDictionary *flickrPropertyList = [NSJSONSerialization JSONObjectWithData:flickrJSONData
                                                                       options:0
                                                                         error:NULL]; //Serialize the json to nsdictionary
    
    return [flickrPropertyList valueForKeyPath:FLICKR_RESULTS_PHOTOS];
}


#pragma mark - NSUrlSession Delegate

//required by protocol
//This method runs when download is done upon task resume
-(void)URLSession:(NSURLSession *)session
     downloadTask:(NSURLSessionDownloadTask *)downloadTask
didFinishDownloadingToURL:(NSURL *)localFile
{
    // we shouldn't assume we're the only downloading going on ...
    if ([downloadTask.taskDescription isEqualToString:FLICKR_FETCH]) {
        // ... but if this is the Flickr fetching, then process the returned data
        [self loadFlickrPhotosFromLocalURL:localFile
                               intoContext:self.photoDatabaseContext
                       andThenExecuteBlock:^{
                           [self flickrDownloadTaskMightBeComplete];
                       }
         ];
    }


}

-(void)URLSession:(NSURLSession *)session
     downloadTask:(NSURLSessionDownloadTask *)downloadTask
didResumeAtOffset:(int64_t)fileOffset
expectedTotalBytes:(int64_t)expectedTotalBytes
{

}

-(void)URLSession:(NSURLSession *)session
     downloadTask:(NSURLSessionDownloadTask *)downloadTask
     didWriteData:(int64_t)bytesWritten
totalBytesWritten:(int64_t)totalBytesWritten
totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
}

-(void) flickrDownloadTaskMightBeComplete
{
    if (self.flickrDownloadBackgroundURLSessionCompletionHandler){
        [self.flickrDownloadSession getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) { //no more download left
            if (![downloadTasks count]){
                void (^completionHandler)() = self.flickrDownloadBackgroundURLSessionCompletionHandler;
                if (completionHandler){
                    completionHandler();
                }
            }
        }];
    }
}
@end
