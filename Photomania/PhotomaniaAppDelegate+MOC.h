//
//  PhotomaniaAppDelegate+MOC.h
//  Photomania
//
//  Created by Mckein on 12/20/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import "PhotomaniaAppDelegate.h"

@interface PhotomaniaAppDelegate (MOC)

-(void)saveContext:(NSManagedObjectContext *)managedObjectContext; //don't need in UIManagedObjectContext in document because it auto saves

-(NSManagedObjectContext *) createMainQueueManagedObjectContext; //created on the main queue like the UIManageDocument

@end
