//
//  PhotosByPhotographerCDTVC.m
//  Photomania
//
//  Created by Mckein on 12/22/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import "PhotosByPhotographerCDTVC.h"

//when there's a public setter you always update the setter for the view to be updated
@interface PhotosByPhotographerCDTVC ()

@end

@implementation PhotosByPhotographerCDTVC

-(void)setPhotographer:(Photographer *)photographer
{
    _photographer = photographer;
    self.title = photographer.name;
    [self setupFetchedResultsController];
}

-(void) setupFetchedResultsController
{
    NSManagedObjectContext *context = self.photographer.managedObjectContext; //get the context by getting the photographer's managedobjectcontext property
    if (context){
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Photo"];
        request.predicate = [NSPredicate predicateWithFormat:@"whoTook = %@", self.photographer];
        request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"title"
                                                                  ascending:YES
                                                                   selector:@selector(localizedStandardCompare:)]];
        
        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                            managedObjectContext:context
                                                                              sectionNameKeyPath:nil
                                                                                       cacheName:nil];
    }else{
        self.fetchedResultsController = nil;
    }
    
}


@end
