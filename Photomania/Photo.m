//
//  Photo.m
//  Photomania
//
//  Created by Mckein on 12/19/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import "Photo.h"
#import "Photographer.h"


@implementation Photo

@dynamic title;
@dynamic subtitle;
@dynamic imageURL;
@dynamic unique;
@dynamic whoTook;

@end
