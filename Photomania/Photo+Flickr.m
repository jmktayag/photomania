//
//  Photo+Flickr.m
//  Photomania
//
//  Created by Mckein on 12/19/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import "Photo+Flickr.h"
#import "FlickrFetcher.h"
#import "Photographer+Create.h"

@implementation Photo (Flickr)

//Takes a flickr dictionary and add a photo object to the database return a pointer to it to me
+(Photo *)photoWithFlickrInfo: (NSDictionary *) photoDictionary
       inManagedObjectContext: (NSManagedObjectContext *) context
{
    Photo *photo = nil;
    
    
    NSString *unique = photoDictionary[FLICKR_PHOTO_ID];
    //Check if the photo is already in the database
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Photo"];
    request.predicate = [NSPredicate predicateWithFormat:@"unique = %@", unique];
    
    NSError *error;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    if (!matches || error || [matches count] > 1){
        //handle error here
    }else if ([matches count]){
        photo = [matches firstObject]; //return found object
    }else{
        //create in db
        photo = [NSEntityDescription insertNewObjectForEntityForName:@"Photo" inManagedObjectContext:context];
        photo.unique = unique;
        photo.title = [photoDictionary valueForKeyPath:FLICKR_PHOTO_TITLE];
        photo.subtitle = [photoDictionary valueForKeyPath:FLICKR_PHOTO_DESCRIPTION];
        photo.imageURL = [[FlickrFetcher URLforPhoto:photoDictionary format:FlickrPhotoFormatLarge] absoluteString];
        
        NSString *photographerName = [photoDictionary valueForKeyPath:FLICKR_PHOTO_OWNER];
        //create or return a photographer from db
        photo.whoTook = [Photographer photographerWithName:photographerName inManagedObjectContext:context];
    }
    
    return photo;
}

//Load photos from array of json response to database
+(void)loadPhotosFromFlickrArray:(NSArray *) photos //of Flickr Dictionary
        intoManagedObjectContext: (NSManagedObjectContext *) context
{
    
    //TODO: extra credit efficient way to check uniques instead of calling and calling nsfetchrequest my propose solution is elimination using nsset -- get the unique of photos in db and check if the ids are in the set.
    for (NSDictionary *photo in photos){
        [self photoWithFlickrInfo:photo inManagedObjectContext:context];
    }
}

@end
