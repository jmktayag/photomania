//
//  URLViewController.h
//  Photomania
//
//  Created by Mckein on 12/22/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface URLViewController : UIViewController

@property (nonatomic, strong) NSURL *url;

@end
