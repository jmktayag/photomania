//
//  Photo+Flickr.h
//  Photomania
//
//  Created by Mckein on 12/19/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import "Photo.h"

@interface Photo (Flickr)

//Takes a flickr dictionary and add a photo object to the database return a pointer to it to me
+(Photo *)photoWithFlickrInfo: (NSDictionary *) photoDictionary
       inManagedObjectContext: (NSManagedObjectContext *) context;

+(void)loadPhotosFromFlickrArray:(NSArray *) photos //of Flickr Dictionary
        intoManagedObjectContext: (NSManagedObjectContext *) context;

@end
