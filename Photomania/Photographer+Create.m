//
//  Photographer+Create.m
//  Photomania
//
//  Created by Mckein on 12/19/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import "Photographer+Create.h"

@implementation Photographer (Create)

//Takes a photographer name and add a photographer object to the database return a pointer to it to me
+(Photographer *)photographerWithName: (NSString *) name
       inManagedObjectContext: (NSManagedObjectContext *) context
{
    Photographer *photographer = nil;
    
    //Check if the photo is already in the database
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Photographer"];
    request.predicate = [NSPredicate predicateWithFormat:@"name = %@", name];
    
    NSError *error;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    if (!matches || error || [matches count] > 1){
        //handle error here
    }else if ([matches count]){
        photographer = [matches lastObject]; //return found object
    }else{
        //create in db
        photographer = [NSEntityDescription insertNewObjectForEntityForName:@"Photographer" inManagedObjectContext:context];
        photographer.name = name;
    }
    
    return photographer;
}

@end
