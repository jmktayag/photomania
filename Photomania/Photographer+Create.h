//
//  Photographer+Create.h
//  Photomania
//
//  Created by Mckein on 12/19/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import "Photographer.h"

@interface Photographer (Create)

//Takes a photographer name and add a photographer object to the database return a pointer to it to me
+(Photographer *)photographerWithName: (NSString *) name
               inManagedObjectContext: (NSManagedObjectContext *) context;

@end
